<?php
require 'vendor/autoload.php';
Mustache_Autoloader::register();
$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/templates')
]);
$productId = $_REQUEST['id'];

$product = json_decode(file_get_contents("http://jdwsearchapi-env-1.mjyhffnp8v.us-east-2.elasticbeanstalk.com/products/${productId}"));

$bodyTemplate = $mustache->loadTemplate('product-page');
$bodyOutput = $bodyTemplate->render($product);

$baseTemplate = $mustache->loadTemplate('base');
echo $baseTemplate->render(['title' => 'Product Page', 'body' => $bodyOutput]);
