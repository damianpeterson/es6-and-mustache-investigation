import Pagination from './Pagination.js';

export default class CategoryPage {
  constructor(renderer, productList, pagination, filter, productCounts) {
    this.renderer = renderer;
    this.productList = productList;
    this.paginationArea = pagination;
    this.productFilter = filter;
    this.productCounts = productCounts;
    this.currentPage = 1;
    this.productsPerPage = 20;
    this.totalProducts = 90;
    this.totalPages = Math.ceil(this.totalProducts / this.productsPerPage);
    this.sort = 'alphabetical';
    this.products = [];

    this.setUpPagination();
    this.setUpFilters();
  }

  setUpPagination() {
    this.pagination = new Pagination(
      this.currentPage,
      this.totalPages,
      event => this.updateProducts(event.target.value, this.sort, this.productsPerPage),
      this.renderer,
    );
    this.paginationTemplate = document.getElementById('pagination-template').innerHTML;
    this.renderPagination();
  }

  renderPagination() {
    this.pagination.currentPage = this.currentPage;
    this.paginationArea.innerHTML = '';
    this.paginationArea.appendChild(this.pagination.render(this.paginationTemplate));
  }

  setUpFilters() {
    this.productFilter.addEventListener('change', (event) => {
      this.updateProducts(this.currentPage, event.target.value, this.productsPerPage);
    });
  }

  // this one uses fetch to get the template content to render
  renderProductCountIndicators() {
    this.productCounts.forEach((productCount) => {
      fetch('/templates/product-count.mustache')
        .then(response => response.text())
        .then((template) => {
          productCount.innerHTML = this.renderer.render(template, {
            count: this.products.length,
          });
        });
    });
  }

  // ...and this one uses an html5 template text area
  renderProducts() {
    const template = document.getElementById('product-item-template').innerHTML;
    let newHtml = '';
    this.products.forEach((product) => {
      newHtml += this.renderer.render(template, { ...product });
    });
    this.productList.innerHTML = newHtml;
  }

  updateProducts(page = 1, sort = 'alphabetical', productsPerPage = 20) {
    const baseUrl = 'http://jdwsearchapi-env-1.mjyhffnp8v.us-east-2.elasticbeanstalk.com';
    const endpoint = `${baseUrl}/products?page=${page}&productsPerPage=${productsPerPage}&sortBy${sort}`;

    fetch(endpoint).then(response => response.json()).then((products) => {
      this.currentPage = page;
      this.sort = sort;
      this.products = products;
      this.renderProducts();
      this.renderProductCountIndicators();
      this.renderPagination();
    });
  }
}
