export default class Pagination {
  constructor(currentPage, totalPages, clickHandler, renderer) {
    this.currentPage = currentPage;
    this.totalPages = totalPages;
    this.clickHandler = clickHandler;
    this.renderer = renderer;
  }

  render(template) {
    const pages = [];
    [...Array(this.totalPages).keys()].forEach((page) => {
      pages.push({
        value: page + 1,
      });
    });
    pages[this.currentPage - 1].disabled = 'disabled';
    const paginationHtml = this.renderer.render(template, {pages});
    const pagination = document.createRange().createContextualFragment(paginationHtml);
    pagination.querySelectorAll('button').forEach((button) => {
      button.addEventListener('click', this.clickHandler);
    });
    return pagination;
  }
}
