import CategoryPage from '../components/CategoryPage.js';

const productList = document.querySelector('.product-list');
const pagination = document.querySelector('.pagination');
const productCountIndicators = document.querySelectorAll('header, footer');
const productFilter = document.querySelector('.product-filters select');

const categoryPage = new CategoryPage(
  Mustache,
  productList,
  pagination,
  productFilter,
  productCountIndicators,
);
