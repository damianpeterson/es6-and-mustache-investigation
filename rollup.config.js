import babel from 'rollup-plugin-babel';
import { uglify } from 'rollup-plugin-uglify';

export default [
  {
    input: 'js/pages/category.js',
    output: {
      file: 'js/legacy/category.min.js',
      format: 'iife',
      name: 'category',
    },
    plugins: [
      babel(),
      uglify(),
    ],
  },
  {
    input: 'js/pages/product.js',
    output: {
      file: 'js/legacy/product.min.js',
      format: 'iife',
      name: 'product',
    },
    plugins: [
      babel(),
      uglify(),
    ],
  },
];
