<?php
require 'vendor/autoload.php';
Mustache_Autoloader::register();
$mustache = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/templates')
]);
$productsPerPage = 20;

$productCountTemplate = $mustache->loadTemplate('product-count');
$productCountOutput = $productCountTemplate->render(['count' => $productsPerPage]);

$filtersTemplate = $mustache->loadTemplate('filters');
$filters = ['options' => [
    ['selected' => 'selected', 'value' => 'alphabetical', 'name' => 'Alphabetical'],
    ['value' => 'sale', 'name' => 'On Sale'],
    ['value' => 'lowest', 'name' => 'Lowest Price'],
    ['value' => 'highest', 'name' => 'Highest Price']
]];
$filtersOutput = $filtersTemplate->render($filters);

$productTemplate = $mustache->loadTemplate('product-item');
$products = json_decode(file_get_contents('http://jdwsearchapi-env-1.mjyhffnp8v.us-east-2.elasticbeanstalk.com/products?page=1&productsPerPage=20'));
$productsOutput = '';
foreach ($products as $product) {
    $productsOutput .= $productTemplate->render($product);
}

$allProducts = json_decode(file_get_contents('http://jdwsearchapi-env-1.mjyhffnp8v.us-east-2.elasticbeanstalk.com/products'));
$paginationTemplate = $mustache->loadTemplate('pagination');
$pages = [];
foreach (range(1, ceil(count($allProducts) / $productsPerPage)) as $pageNumber) {
    $page = ['value' => $pageNumber];
    if ($pageNumber == 1) {
        $page['disabled'] = 'disabled';
    }
    $pages['pages'][] = $page;
}
$paginationOutput = $paginationTemplate->render($pages);

$productTemplateRaw = file_get_contents(__DIR__ . '/templates/product-item.mustache');
$paginationTemplateRaw = file_get_contents(__DIR__ . '/templates/pagination.mustache');

$bodyTemplate = $mustache->loadTemplate('category-page');
$bodyOutput = $bodyTemplate->render([
    'productCount' => $productCountOutput,
    'filters' => $filtersOutput,
    'products' => $productsOutput,
    'pagination' => $paginationOutput,
    'productTemplate' => $productTemplateRaw,
    'paginationTemplate' => $paginationTemplateRaw,
]);

$baseTemplate = $mustache->loadTemplate('base');
echo $baseTemplate->render(['title' => 'Category Page', 'body' => $bodyOutput]);
